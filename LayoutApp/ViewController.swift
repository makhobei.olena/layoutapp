//
//  ViewController.swift
//  LayoutApp
//
//  Created by Oleh Makhobei on 02.08.2021.
//

import UIKit

class ViewController: UIViewController {
    var textData : String? = nil
    var centerLabel:UILabel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        let dataLbl = UILabel()
        dataLbl.text = ""
        dataLbl.translatesAutoresizingMaskIntoConstraints = false
        dataLbl.backgroundColor = .lightGray
        dataLbl.numberOfLines = 0
        dataLbl.layer.masksToBounds = true
        dataLbl.layer.cornerRadius = 5
        
        self.centerLabel = dataLbl
        
        let clearButton = UIButton()
        clearButton.setTitle("Clear", for: .normal)
        clearButton.backgroundColor = .black
        clearButton.translatesAutoresizingMaskIntoConstraints = false
        clearButton.layer.cornerRadius = 10
        
        clearButton.addTarget(self, action: #selector(clearBttAction), for: .touchUpInside)
        
        let loadButton = UIButton()
        loadButton.backgroundColor = .black
        loadButton.setTitle("Load", for: .normal)
        loadButton.translatesAutoresizingMaskIntoConstraints = false
        loadButton.layer.cornerRadius = 10
            
        loadButton.addTarget(self, action: #selector(loadBttAction), for: .touchUpInside)
        
        self.view.addSubview(dataLbl)
        self.view.addSubview(clearButton)
        self.view.addSubview(loadButton)
        
        let viewsDict = ["centerLbl": dataLbl,"loadButton": loadButton, "clearButton": clearButton]
        
        var viewsConstraints = [NSLayoutConstraint]()
        
    //Adding constraints
        let centerLblHorizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[centerLbl]-25-|", options: [], metrics: nil, views: viewsDict)
        let centerLblVerticalContraints = NSLayoutConstraint.constraints(withVisualFormat:"V:|-50-[centerLbl]-(>=100)-|", options: [], metrics: nil, views: viewsDict)
        let clearBttVerticalConstraint =
            NSLayoutConstraint.constraints(withVisualFormat: "V:[clearButton(50)]-10-|", options: [], metrics: nil, views: viewsDict)
        let loadBttVerticalConstraint =
            NSLayoutConstraint.constraints(withVisualFormat: "V:[loadButton(50)]-10-|", options: [], metrics: nil, views: viewsDict)
        let horizontalButtonGroupConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[clearButton(==loadButton)]-50-[loadButton(==clearButton)]-10-|", options: [], metrics: nil, views: viewsDict)
        
        viewsConstraints += centerLblHorizontalConstraints
        viewsConstraints += centerLblVerticalContraints
        viewsConstraints += clearBttVerticalConstraint
        viewsConstraints += loadBttVerticalConstraint
        viewsConstraints += horizontalButtonGroupConstraints
        
        NSLayoutConstraint.activate(viewsConstraints)

        // Do any additional setup after loading the view.
    }

    @objc func clearBttAction(sender: UIButton!){
        centerLabel?.text = ""
    }
    
    @ objc func loadBttAction(sender: UIButton!){
        centerLabel?.text = textData
    }
    
    func loadData(){
        
        if let filePath = Bundle.main.path(forResource: "texts", ofType: "txt"){
            if let texts = try? String(contentsOfFile: filePath){
                textData = texts
                print ("allWords count = \(String(describing: textData?.count))")
                
                
            }else{
                print("Failed to load texts.txt")
            }
        }
    }
    
}

